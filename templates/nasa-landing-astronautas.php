<?php
/**
* El template para mostrar el landing
*/
get_header();
$imgLoading = plugin_dir_url( __FILE__ ) . '../templates/images/loading2.gif';
$toolsURL = plugin_dir_url( __FILE__ ) . '../includes/tools-nasa-astronautas.php';
?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <form class="form-datos pt-4" id="form-datos">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="txt-nombre">Nombre</label>
                            <input type="text" class="form-control" id="txt-nombre" aria-describedby="help-nombre" placeholder="Escribe tu nombre" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="txt-edad">Edad</label>
                            <input type="number" class="form-control" id="txt-edad" aria-describedby="help-nombre" placeholder="Escribe tu edad" required min="18">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="txt-sexo">Sexo</label>
                            <select class="form-control" id="txt-sexo">
                                <option value="femenino">Femenino</option>
                                <option value="masculino">Masculino</option>
                                <option value="otro">Otro</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="txt-email">Correo electrónico</label>
                            <input type="email" class="form-control" id="txt-email" aria-describedby="help-email" placeholder="Escribe tu correo" required>
                            <small id="help-email" class="form-text text-muted">Nunca compartiremos tu correo con nadie mas.</small>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="txt-motivo">Motivo para ir a la Luna</label>
                            <textarea class="form-control" id="txt-motivo" rows="3" placeholder="Escribe los motivos que tienes para ir a la luna"></textarea>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label for="txt-ultimo-contacto">Último contacto con extraterrestres</label>
                            <textarea class="form-control" id="txt-ultimo-contacto" rows="3" placeholder="Escribe cuando fue tu último contacto con extraterrestres"></textarea>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-check form-group">
                            <input type="checkbox" class="form-check-input" id="chk-acepto-terminos" required>
                            <label class="form-check-label" for="chk-acepto-terminos">Acepto términos y condiciones</a></label>
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="div_screen_loading">
    <div class="div_inner_screen_loading top high"></div>
    <img class="img_screen_loading highest" src="<?php echo $imgLoading; ?>">
    <div class="div_inner_screen_loading bottom high"></div>
</div>
<?php
get_footer();
?>
<script type="text/javascript">
	toolsURL = "<?php echo $toolsURL; ?>";
</script>

