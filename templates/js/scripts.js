var screenWidth, screenHeight, bodyHeight;
var themeDirectory, toolsURL;


$(document).ready(function() {
  createVars(); // cache de variables
  setListeners(); // llamado de objetos
  resize();
});

function getScrollBarWidth () {
  var inner = document.createElement('p');
  inner.style.width = "100%";
  inner.style.height = "200px";

  var outer = document.createElement('div');
  outer.style.position = "absolute";
  outer.style.top = "0px";
  outer.style.left = "0px";
  outer.style.visibility = "hidden";
  outer.style.width = "200px";
  outer.style.height = "150px";
  outer.style.overflow = "hidden";
  outer.appendChild (inner);

  document.body.appendChild (outer);
  var w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  var w2 = inner.offsetWidth;
  if (w1 == w2) w2 = outer.clientWidth;

  document.body.removeChild (outer);
  return (w1 - w2);
};

function createVars(){

}

window.onorientationchange = resize;
$(window).resize(function() {
  resize();
});

function resize(){
  screenWidth = $(window).width() + getScrollBarWidth();
  screenHeight = $(window).height();
  console.log("screenWidth: "+screenWidth);
  console.log("screenHeight: "+screenHeight);
}

function setListeners(){

}

/*FUNCIONES*/ 
//Envía el formulario de contacto
$('#form-datos').submit(function(e) {
    e.preventDefault();
    var url = toolsURL;
    var formulario = $('#form-datos');

    var nombre = $("#txt-nombre").val();
    var email = $("#txt-email").val();
    var edad = $("#txt-edad").val(); 
    var sexo = $("#txt-sexo").val();
    var motivo = $("#txt-motivo").val();
    var ultimoContacto = $("#txt-ultimo-contacto").val(); 
    var request = "crearAstronauta";

    var parametros = {
        "nombre": nombre,
        "email": email,
        "edad": edad,
        "sexo": sexo,
        "motivo": motivo,
        "ultimoContacto": ultimoContacto,
        "request" : request
    };
    $.ajax({
        data: parametros,
        url: url,
        type: 'post',
        beforeSend: function () {
          showLoadingScreen();
        },
        success: function (response) {
		    var arr = response.split("|");
		    var estado = arr[0];
        var respuesta = arr[1];
          if(estado == '@ER'){		  
            console.log(estado+' Respuesta: '+respuesta);
          }else {
            console.log(estado+' Respuesta: '+respuesta);
          }
          formulario[0].reset();
          showLoadingScreen();
        }
    });
});

//Muestra u oculta la cortina de espera mientras pasa a una nueva pregunta
function showLoadingScreen(){
  $('.div_screen_loading').toggleClass('active');
	$('.div_inner_screen_loading').toggleClass('active');
	$('.img_screen_loading').toggleClass('active');
}