<?php
//Función que crea las tablas en la base de datos
function nasa_crear_tablas_bd() {
    global $wpdb;
    // Definimos el nombre de la tabla con el prefijo usado en la instalación:
    $astronautas = $wpdb->prefix . 'NASA_astronautas';
    $charset_collate = $wpdb->get_charset_collate();
    // Diseñamos la consulta SQL para la nueva tabla:
    $sql = "CREATE TABLE $astronautas (
        id int(9) NOT NULL AUTO_INCREMENT,
        post_id int(9) NOT NULL,
        nombre varchar(100) NOT NULL,
        edad int(9) NOT NULL,
        sexo varchar(100),
        correo varchar(100),
        motivo_para_ir varchar(255),
        ultimo_contacto varchar(100),
        UNIQUE KEY id(id)
        ) $charset_collate;";
   
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
    // Ejecutamos la consulta:
    dbDelta($sql);
}

//Función que crea el menú del plugin en el escritorio
function nasa_add_desktop_menu(){

    add_menu_page('NASA Clientes Dashboard', //Título de la página
        'NASA Clientes', //Título del menú
        'manage_options', //Rol que puede acceder
        'nasa-menu', //Slug página
        'nasa_renderizar_configuracion', //Función que se llama y pinta el contenido
        '', //Icon URL
        null //posición en el menú (orden)
    );
    
    
    add_submenu_page('nasa-menu', //Slug padre
        'Configuración', //Título del menú
        'Configuración', //Título del menú
        'manage_options', //Rol que puede acceder
        'nasa-menu-configuracion', //Slug página
        'nasa_renderizar_configuracion', //Función que se llama y pinta el contenido
        null
    ); 

    add_submenu_page('nasa-menu', //Slug padre
        'Reporte', //Título del menú
        'Reporte', //Título del menú
        'manage_options', //Rol que puede acceder
        'edit.php?post_type=astronauta', //Slug página
        '', //Función que se llama y pinta el contenido
        null
    );

}
add_action('admin_menu', 'nasa_add_desktop_menu');

//Función que renderiza la ventana de configuración
function nasa_renderizar_configuracion(){
	if(current_user_can("manage_options") ) {
	?>
	<div class="wrap">
		<h2>Configuración</h2>
		Bienvenido a la configuración de ejemplo
	</div>
    <?php
	}else{

	}   
}

//Función que renderiza la ventana de reporte
function nasa_renderizar_reporte(){
	if(current_user_can("manage_options") ) {
	?>
	<div class="wrap">
		<h2>Reporte</h2>
		Bienvenido a los reportes de ejemplo
	</div>
    <?php
	}else{

	}   
}

add_action( 'init', 'create_post_types' );
// Crear POST
function create_post_types() {
	//Post Astronautas
	$args = array(
		'labels' => array(
			'name' => __( 'Astronautas' ),
			'singular_name' => __( 'Astronauta' )
		),
		'public' => true,
		'has_archive' => true,
		//'show_ui' => false,
		'show_in_menu' => false,
		'capability_type' => 'post',
		'supports' => array('title')
	);
	register_post_type( 'astronauta', $args);
}

function  home_page_template ( $template ) {
    if ( is_front_page() ) {
        $new_template = plugin_dir_path( __FILE__ ) . '../templates/nasa-landing-astronautas.php';
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }
    return $template;
}
add_filter( 'template_include', 'home_page_template', 99 );


//Función que agrega css y js a los archivos del template
function add_custom_styles() {
    wp_register_style('bootstrap', plugin_dir_url( __FILE__ ) . '../templates/css/bootstrap.min.css');
    wp_register_style('nasa-main-style', plugin_dir_url( __FILE__ ) . '../templates/css/main.css');
    
    wp_register_script('nasa-jquery', plugin_dir_url( __FILE__ ) . '../templates/js/jquery.min.js', '', '', true);
    wp_register_script('bootstrap', plugin_dir_url( __FILE__ ) . '../templates/js/bootstrap.min.js', '', '', true);
    wp_register_script('nasa-scripts', plugin_dir_url( __FILE__ ) . '../templates/js/scripts.js', '', '', true);

    wp_enqueue_style('bootstrap');
    wp_enqueue_style('nasa-main-style');

    wp_enqueue_script('nasa-jquery');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('nasa-scripts');
}
add_action( 'wp_enqueue_scripts', 'add_custom_styles' );



