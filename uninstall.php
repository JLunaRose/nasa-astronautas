<?php
/*
function nasa_eliminar_tablas_bd(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'NASA_astronautas';
    $sql = "DROP TABLE IF EXISTS $table_name";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
    // Ejecutamos la consulta:
    dbDelta($sql);
}
//require_once plugin_dir_path(__FILE__) . 'includes/functions-nasa-astronautas.php';
register_uninstall_hook(__FILE__, 'nasa_eliminar_tablas_bd');
*/

if( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit();
global $wpdb;
$table_name = $wpdb->prefix . 'NASA_astronautas';
$wpdb->query( "DROP TABLE IF EXISTS $table_name" );
delete_option("my_plugin_db_version");
