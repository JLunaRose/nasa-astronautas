<?php
/*
 * Plugin Name: NASA_astronautas
 * Description: Plugin de prueba, ejemplo astronautas Gradiweb
 * Version: 1.0.0
 * Author: Luna Rose
 * Author URI: https://www.linkedin.com/in/jlunarose/
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

require_once plugin_dir_path(__FILE__) . 'includes/functions-nasa-astronautas.php';
register_activation_hook(__FILE__, 'nasa_crear_tablas_bd');
?>